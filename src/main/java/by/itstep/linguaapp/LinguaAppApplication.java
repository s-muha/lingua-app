package by.itstep.linguaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinguaAppApplication {



	public static void main(String[] args) {
		SpringApplication.run(LinguaAppApplication.class, args);

	}
}
