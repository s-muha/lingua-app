package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;


    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public List<UserFullDto> findALL(){
        return userService.findAll();
    }

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto dto){
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@Valid @RequestBody UserUpdateDto dto){
        return userService.update(dto);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id) {
        userService.delete(id);
    }

    @PutMapping("/users/role")
    public UserFullDto changeRole(@Valid @RequestBody ChangeRoleDto dto) {
        return userService.changeRole(dto);
    }

    @PutMapping("/users/password")
    public void changePassword(@Valid @RequestBody ChangeUserPasswordDto dto) {
        userService.changePassword(dto);
    }

    @PutMapping("/users/{id}/block")
    public void block(@PathVariable Integer id, @RequestHeader("Authorization") String authorization) throws Exception{
        userService.block(id);
    }

}
