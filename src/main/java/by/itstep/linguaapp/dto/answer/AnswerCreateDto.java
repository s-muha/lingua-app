package by.itstep.linguaapp.dto.answer;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AnswerCreateDto {

    @NotNull
    private String body;

    @NotNull
    private Boolean correct;

}
