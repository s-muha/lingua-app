package by.itstep.linguaapp.dto.answer;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AnswerUpdateDto {

    @NotNull
    private Integer id;

    @NotNull
    private String body;

    @NotNull
    private Boolean correct;
}
