package by.itstep.linguaapp.dto.categoty;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CategoryCreateDto {

    @NotBlank
    private String name;
}
