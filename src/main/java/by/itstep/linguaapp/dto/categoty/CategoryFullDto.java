package by.itstep.linguaapp.dto.categoty;

import lombok.Data;

@Data
public class CategoryFullDto {

    private Integer id;
    private String name;

}
