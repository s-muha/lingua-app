package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.categoty.CategoryFullDto;
import by.itstep.linguaapp.entity.enums.QuestionLevel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionFullDto {

    private Integer id;
    private String description;
    private QuestionLevel level;
    private List<AnswerFullDto> answers = new ArrayList<>();
    private List<CategoryFullDto> categories = new ArrayList<>();
}
