package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.enums.UserRole;
import lombok.Data;

@Data
public class UserFullDto {

    private Integer id;
    private String name;
    private String email;
    private String country;
    private String phone;
    private UserRole role;
}
