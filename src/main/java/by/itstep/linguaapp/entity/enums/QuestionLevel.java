package by.itstep.linguaapp.entity.enums;

public enum QuestionLevel {
    A1, A2, B1, B2, C1, C2;
}
