package by.itstep.linguaapp.entity.enums;

public enum UserRole {

    USER, ADMIN;
}
