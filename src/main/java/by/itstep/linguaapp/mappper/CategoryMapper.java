package by.itstep.linguaapp.mappper;

import by.itstep.linguaapp.dto.categoty.CategoryCreateDto;
import by.itstep.linguaapp.dto.categoty.CategoryFullDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    CategoryFullDto map(CategoryEntity entity);

    CategoryEntity map(CategoryCreateDto dto);

    List<CategoryFullDto> map(List<CategoryEntity> entity);
}
