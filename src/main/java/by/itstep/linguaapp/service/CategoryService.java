package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.categoty.CategoryCreateDto;
import by.itstep.linguaapp.dto.categoty.CategoryFullDto;
import by.itstep.linguaapp.dto.categoty.CategoryUpdateDto;

import java.util.List;

public interface CategoryService {

    CategoryFullDto create(CategoryCreateDto dto);

    CategoryFullDto update(CategoryUpdateDto dto);

    CategoryFullDto findById(int id);

    List<CategoryFullDto> findAll();

    void delete(int id);

}
