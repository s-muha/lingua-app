package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.answer.AnswerUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.BusinessLogicException;
import by.itstep.linguaapp.mappper.AnswerMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    AnswerRepository answerRepository;
    @Autowired
    AnswerMapper answerMapper;

    @Override
    @Transactional
    public AnswerFullDto update(AnswerUpdateDto dto) {
        AnswerEntity answerToUpdate = answerRepository.getById(dto.getId());
        if (answerToUpdate == null) {
            throw new AppEntityNotFoundException("Answer was not found by id: " + dto.getId());
        }
        if (!dto.getCorrect() && answerToUpdate.getCorrect()) {
            throw new BusinessLogicException("Current answer is correct. Set other answer as correct before the update");
        }
        if (dto.getCorrect() && !answerToUpdate.getCorrect()) {
            removeCorrectFlagFromAnswer(answerToUpdate.getQuestion());
        }
        answerToUpdate.setCorrect(dto.getCorrect());
        answerToUpdate.setBody(dto.getBody());
        AnswerEntity updatedAnswer = answerRepository.save(answerToUpdate);
        System.out.println("AnswerServiceImpl -> Answer was successfully updated");
        return answerMapper.map(updatedAnswer);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        AnswerEntity answerToDelete = answerRepository.getById(id);
        if (answerToDelete == null) {
            throw new AppEntityNotFoundException("Answer was not found by id: " + id);
        }
        if (answerToDelete.getCorrect()) {
            throw new BusinessLogicException("Current answer is correct. Set other answer as correct before the delete");
        }
        if (answerToDelete.getQuestion().getAnswers().size() < 3) {
            throw new BusinessLogicException("Question must contains at least 2 answers");
        }
        answerToDelete.setDeletedAt(Instant.now());

        answerRepository.save(answerToDelete);
    }

    private void removeCorrectFlagFromAnswer(QuestionEntity question) {
//        for (AnswerEntity answer : question.getAnswers()) {
//            if (answer.getCorrect()) {
//                answer.setCorrect(false);
//                answerRepository.save(answer);
//                break;
//            }
//        }
        question.getAnswers().stream()
                .filter(answer -> answer.getCorrect() == true)
                .peek(answer -> answer.setCorrect(false))
                .forEach(answer -> answerRepository.save(answer));
    }
}
