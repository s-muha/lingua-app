package by.itstep.linguaapp.service.impl;


import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.enums.UserRole;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValuesIsTakenException;
import by.itstep.linguaapp.exception.WrongUserPasswordException;
import by.itstep.linguaapp.mappper.UserMapper;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {

        UserEntity entityToSave = userMapper.map(createDto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(UserRole.USER);
        createDto.setPassword(passwordEncoder.encode(createDto.getPassword()) + createDto.getEmail());

        Optional<UserEntity> entityWithSameEmail =
                userRepository.findByEmail(entityToSave.getEmail());

        if (entityWithSameEmail.isPresent()) {
            throw new UniqueValuesIsTakenException("Email is taken");
        }
        UserEntity savedEntity = userRepository.save(entityToSave);
        UserFullDto userDto = userMapper.map(savedEntity);
        System.out.println("UserServiceImpl -> user was successfully created");
        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {

        UserEntity userToUpdate = userRepository.findById(dto.getId()).
                orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setName(dto.getName());
        userToUpdate.setCountry(dto.getCountry());
        userToUpdate.setPhone(dto.getPhone());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);
        System.out.println("UserServiceImpl -> user was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {
        UserEntity foundUser = userRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        UserFullDto userDto = userMapper.map(foundUser);
        System.out.println("UserServiceImpl -> user was successfully found");
        return userDto;
    }

    @Override
    @Transactional
    public List<UserFullDto> findAll() {
        List<UserEntity> foundUsers = userRepository.findAll();
        List<UserFullDto> dtos = userMapper.map(foundUsers);
        System.out.println("UserServiceImpl -> " + dtos.size() + " users were found");
        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        UserEntity foundUser = userRepository.findById(id).
                orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));
        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);
        System.out.println("UserServiceImpl -> User was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(ChangeUserPasswordDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getUserId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId());
        }
        if (!userToUpdate.getPassword().equals(passwordEncoder.encode(dto.getOldPassword()) + userToUpdate.getEmail())) {
            throw new WrongUserPasswordException("Wrong password");
        }
        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getEmail()));
        userRepository.save(userToUpdate);
        System.out.println("UserServiceImpl -> User password was successfully update");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeRoleDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getUserId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId());
        }
        userToUpdate.setRole(dto.getNewRole());
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);
        System.out.println("UserServiceImpl -> User role was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public void block(Integer userId) {
        UserEntity entityToUpdate = userRepository.getById(userId);
        if (entityToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + userId);
        }
        entityToUpdate.setBlocked(true);
        userRepository.save(entityToUpdate);
        System.out.println("UserServiceImpl -> User was successfully updated.");
    }

    private boolean currentUserIsAdmin() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        for (GrantedAuthority authority : authentication.getAuthorities()) {
//            if (authority.getAuthority().equals("ROLE_" + UserRole.ADMIN.name())) {
//                return true;
//            }
//        }
//        return false;

        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(ga -> ga.getAuthority().equals("ROLE_" + UserRole.ADMIN.name()));


    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }
}
