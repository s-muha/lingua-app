package by.itstep.linguaapp.utils;


import by.itstep.linguaapp.dto.user.UserCreateDto;

import static by.itstep.linguaapp.LinguaappApplicationTests.FAKER;

public class DtoUserHelper {

    public static UserCreateDto generateUserCreateDto(){
        UserCreateDto createDto = new UserCreateDto();

        createDto.setEmail(FAKER.internet().emailAddress());
        createDto.setName(FAKER.name().firstName());
        createDto.setPassword(FAKER.internet().password());
        createDto.setCountry(FAKER.country().name());
        createDto.setPhone(FAKER.phoneNumber().cellPhone());
        return createDto;
    }
}
